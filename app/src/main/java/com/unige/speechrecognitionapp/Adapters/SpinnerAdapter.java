package com.unige.speechrecognitionapp.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jwang123.flagkit.FlagKit;
import com.unige.speechrecognitionapp.Models.Country;
import com.unige.speechrecognitionapp.R;

import java.util.ArrayList;

public class SpinnerAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Country> countries;
    private LayoutInflater inflter;

    public SpinnerAdapter(Context applicationContext, ArrayList<Country> countries) {
        this.context = applicationContext;
        this.countries = countries;
        this.inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return countries.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_row, null);
        TextView name =  view.findViewById(R.id.textView);

        name.setText(countries.get(i).getLocale());
        return view;
    }
}