package com.unige.speechrecognitionapp.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.unige.speechrecognitionapp.Models.Note;
import com.unige.speechrecognitionapp.R;
import com.unige.speechrecognitionapp.RecognitionAppClickListener;

import java.util.ArrayList;

public class NotesAdapter  extends RecyclerView.Adapter{
    private ArrayList<Note> notes;
    private Activity activity;
    private RecognitionAppClickListener onClickListener;


    public NotesAdapter(ArrayList<Note> notes, Activity activity, RecognitionAppClickListener onClickListener) {
        this.notes = notes;
        this.activity = activity;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemLayoutView;
        final RecyclerView.ViewHolder viewHolder;

        itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_row, parent, false);
        viewHolder = new NoteViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((NoteViewHolder) holder).tvNote.setText(notes.get(position).getText());
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    private class NoteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView tvNote;
        private final ImageView ivSpeak, ivShare, ivDelete;

        public NoteViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            itemLayoutView.setOnClickListener(this);
            tvNote = itemLayoutView.findViewById(R.id.tv_note);
            ivSpeak = itemLayoutView.findViewById(R.id.iv_speak);
            ivShare = itemLayoutView.findViewById(R.id.iv_share);
            ivDelete = itemLayoutView.findViewById(R.id.iv_delete);

            ivSpeak.setOnClickListener(this);
            ivShare.setOnClickListener(this);
            ivDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            NotesAdapter.this.onClickListener.clicked(view, notes.get(getAdapterPosition()));
        }
    }
}
