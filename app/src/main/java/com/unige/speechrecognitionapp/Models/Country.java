package com.unige.speechrecognitionapp.Models;

public class Country  implements Comparable {

    private String locale;
    private String code;

    public Country(String locale, String code) {

        this.locale = locale;
        this.code = code;
    }


    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public int compareTo(Object o) {

        /* For Ascending order*/
        return this.locale.compareTo(((Country)o).locale);
    }
}
