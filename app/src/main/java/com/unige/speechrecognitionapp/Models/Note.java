package com.unige.speechrecognitionapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import io.realm.RealmObject;

public class Note extends RealmObject implements Parcelable {
    private String text;
    private String languageCode;
    public Note() {
    }
    public Note(String text, String languageCode) {
        this.text = text;
        this.languageCode = languageCode;
    }

    protected Note(Parcel in) {
        text = in.readString();
        languageCode = in.readString();
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(text);
        parcel.writeString(languageCode);
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }
}
