package com.unige.speechrecognitionapp;

import android.view.View;
import android.widget.CompoundButton;

public interface RecognitionAppClickListener {
    void clicked(View v, Object object);

}
