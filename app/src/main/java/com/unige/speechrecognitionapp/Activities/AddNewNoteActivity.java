package com.unige.speechrecognitionapp.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.unige.speechrecognitionapp.Adapters.SpinnerAdapter;
import com.unige.speechrecognitionapp.Helpers.AppConstants;
import com.unige.speechrecognitionapp.Models.Country;
import com.unige.speechrecognitionapp.Models.Note;
import com.unige.speechrecognitionapp.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.Objects;

public class AddNewNoteActivity extends RecognitionAppGeneralActivity implements  View.OnClickListener, AdapterView.OnItemSelectedListener {

    private static final int MY_PERMISSIONS_RECORD_AUDIO = 1;
    private static final int REQUEST_CODE_SPEECH_INPUT = 2;

    private EditText results;
    private Intent mSpeechRecognizerIntent;
    private FloatingActionButton fab;
    private ArrayList<Country> countries;
    private Country selectedCountry;
    private Spinner spin;
    private Button btnSave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_note);
        getSupportActionBar().setTitle(R.string.add_new_note);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        checkPermission();
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);
        fab.setEnabled(false);
        results = findViewById(R.id.results);

        spin = findViewById(R.id.simpleSpinner);
        spin.setOnItemSelectedListener(this);


        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);

        String[] languages = Locale.getISOLanguages();
        countries = new ArrayList<>();

        for (String language : languages) {
            Locale locale = new Locale(language);

            locale.getISO3Language();
            countries.add(new Country(locale.getDisplayLanguage(),  locale.getLanguage()));

        }
        if(countries.size()>0) {
            Collections.sort(countries);
            selectedCountry = countries.get(0);
            fab.setEnabled(true);
            SpinnerAdapter customAdapter=new SpinnerAdapter(getApplicationContext(),countries);
            spin.setAdapter(customAdapter);

            mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            mSpeechRecognizerIntent.putExtra("android.speech.extra.EXTRA_ADDITIONAL_LANGUAGES", new String[]{selectedCountry.getCode()});

            mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                  selectedCountry.getCode());
        }



    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_RECORD_AUDIO);

            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_RECORD_AUDIO: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                } else {

                    Toast.makeText(this, "Permissions Denied to record audio", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }




    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.fab){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_RECORD_AUDIO);

                }else{
                    startActivityForResult(mSpeechRecognizerIntent, REQUEST_CODE_SPEECH_INPUT);
                }

            }
        }else if(id == R.id.btn_save){
            if(!results.getText().toString().isEmpty()) {
                if (!mRealm.isClosed()) {
                    mRealm.executeTransaction(realm -> {

                        mRealm.insertOrUpdate(new Note(results.getText().toString(), selectedCountry.getCode()));
                    });
                }
                Intent intent = new Intent();
                intent.putExtra(AppConstants.ADD_NOTE_EXTRA, new Note(results.getText().toString(), selectedCountry.getCode()));
                setResult(RESULT_OK, intent);
                finish();
            }else Toast.makeText(getApplicationContext(), R.string.cannot_add_empty_note, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        selectedCountry = countries.get(i);
        if(selectedCountry!=null) {
            fab.setEnabled(true);
            mSpeechRecognizerIntent.putExtra("android.speech.extra.EXTRA_ADDITIONAL_LANGUAGES", new String[]{selectedCountry.getCode()});

            mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                    selectedCountry.getCode());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

        if(selectedCountry==null){
            fab.setEnabled(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SPEECH_INPUT) {
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> result = data.getStringArrayListExtra(
                        RecognizerIntent.EXTRA_RESULTS);
                results.setText(
                        Objects.requireNonNull(result).get(0));
            }
        }
    }
}
