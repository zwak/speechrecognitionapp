package com.unige.speechrecognitionapp.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.unige.speechrecognitionapp.Adapters.NotesAdapter;
import com.unige.speechrecognitionapp.Helpers.AppConstants;
import com.unige.speechrecognitionapp.Models.Note;
import com.unige.speechrecognitionapp.R;
import com.unige.speechrecognitionapp.RecognitionAppClickListener;

import java.util.ArrayList;
import java.util.Locale;

import io.realm.RealmResults;

public class HomeActivity extends RecognitionAppGeneralActivity implements View.OnClickListener, RecognitionAppClickListener {



    private FloatingActionButton fab;
    private ArrayList<Note>notes = new ArrayList<>();
    private NotesAdapter notesAdapter;
    private RecyclerView rvNotes;
    private TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().setTitle(R.string.menu_home);
        init();
    }

    void init(){
        tts = new TextToSpeech(getApplicationContext(), status -> {

        });
        rvNotes = findViewById(R.id.rv_notes);
        rvNotes.setLayoutManager(new LinearLayoutManager(this));
        notesAdapter = new NotesAdapter(notes, this, this);
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);

        if (!mRealm.isClosed()){
            mRealm.executeTransaction(realm -> {
                RealmResults<Note> noteRealmResults = mRealm.where(Note.class).findAll();
                notes.addAll(realm.copyFromRealm(noteRealmResults));
                notesAdapter = new NotesAdapter(notes, this, this);
                rvNotes.setAdapter(notesAdapter);
            });
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.fab) {
            Intent intent = new Intent(this, AddNewNoteActivity.class);
            ((RecognitionAppGeneralActivity)this).activityLauncher.launch(intent, result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    Note note = data.getParcelableExtra(AppConstants.ADD_NOTE_EXTRA);
                    notes.add(0, note);
                    notesAdapter.notifyItemInserted(0);
                    rvNotes.scrollToPosition(0);
                }
            });
        }
    }


    @Override
    public void clicked(View v, Object object) {
        int id = v.getId();
        Note note = (Note) object;
        if(id==R.id.iv_speak) {
            if (tts.isLanguageAvailable(new Locale(note.getLanguageCode()))==TextToSpeech.LANG_AVAILABLE) {
                tts.setLanguage(Locale.forLanguageTag(note.getLanguageCode()));
                tts.speak(note.getText(), TextToSpeech.QUEUE_FLUSH, null);
            }else Toast.makeText(getApplicationContext(), R.string.error_occured, Toast.LENGTH_SHORT).show();

            if(note.getText()!=null && !note.getLanguageCode().isEmpty()) {

            }else Toast.makeText(getApplicationContext(), R.string.cannot_speak_this_note, Toast.LENGTH_SHORT).show();
        }else if (id== R.id.iv_share){

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, note.getText());
            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);

        }else if (id==R.id.iv_delete){
            if (!mRealm.isClosed()){
                mRealm.executeTransaction(realm -> {
                    RealmResults<Note> rows=  mRealm.where(Note.class).equalTo("text", note.getText()).findAll();
                    rows.deleteAllFromRealm();
                    int index = notes.indexOf(note);
                    notes.remove(note);
                    notesAdapter.notifyItemRemoved(index);
                });
            }
        }

    }
}