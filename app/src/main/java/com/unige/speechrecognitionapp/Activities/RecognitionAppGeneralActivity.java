package com.unige.speechrecognitionapp.Activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.unige.speechrecognitionapp.RecognitionAppActivityResult;
import com.unige.speechrecognitionapp.RecognitionAppController;

import java.util.Random;

import io.realm.Realm;


public class RecognitionAppGeneralActivity extends AppCompatActivity implements Thread.UncaughtExceptionHandler {
    private static final Random RANDOM = new Random();
    public Realm mRealm;

    public final RecognitionAppActivityResult<Intent, ActivityResult> activityLauncher = RecognitionAppActivityResult.registerActivityForResult(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setFitsSystemWindows(true);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mRealm = Realm.getInstance(RecognitionAppController.realmConfig);



        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {

        throwable.printStackTrace(); // not all Android versions will print the stack trace automatically

        Intent intent = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

        System.exit(1); // kill off the crashed app
    }
}
