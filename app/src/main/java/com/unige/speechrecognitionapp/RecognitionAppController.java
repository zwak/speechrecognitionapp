package com.unige.speechrecognitionapp;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class RecognitionAppController extends Application {
    public static final boolean DEBUG_MODE = false;

    private static RecognitionAppController sInstance;
    private static Activity activeActivity;
    public static RealmConfiguration realmConfig;

    public static synchronized RecognitionAppController getInstance() {
        return sInstance;
    }



    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        Realm.init(getApplicationContext());

        realmConfig = new RealmConfiguration.Builder()
                .name("RecognitionApp.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .allowQueriesOnUiThread(true)
                .allowWritesOnUiThread(true)
                .build();
        Realm.setDefaultConfiguration(realmConfig);
        setupActivityListener();
    }

    /*@Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }*/

    @Override
    public void onTerminate() {
        super.onTerminate();
        Realm.getDefaultInstance().close();
    }

    private void setupActivityListener() {
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {
                activeActivity =activity;
            }

            @Override
            public void onActivityPaused(Activity activity) {
                activeActivity =activity;
            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
            }
        });
    }

    public static Activity getActiveActivity() {
        return activeActivity;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }
}
